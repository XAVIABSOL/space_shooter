﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class Background : MonoBehaviour {
 
    private Material mat;
    public float smooth = 1.0f;
    private float offsetx = 0.0f;
 
    private Vector2 textoffset;
 
    private float axisx;

    // Use this for initialization
    void Awake () {
        mat = GetComponent<Renderer> ().material;
        textoffset = new Vector2 (0, offsetx);
    }
   
    // Update is called once per frame
    void Update () {
        offsetx += (Time.deltaTime * (smooth + 75*axisx/100));
        if(offsetx >= 100) offsetx -= 100;
 
        textoffset.x = offsetx;
        mat.SetTextureOffset("_MainTex", textoffset);
    }

    public void SetVelocity(float valx){
        axisx = valx;
    }
}